package com.qmdm.clinet.comtroller;

import com.qmdm.api.pojo.User;
import com.qmdm.clinet.service.ClientUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by chenjin on 17/12/18.
 */
@RestController
public class UserInfoController {

    @Autowired
    private ClientUserService clientUserService;

    @Value("${DOM_PATH}")
    private String path;

    @ResponseBody
    @RequestMapping("/user/get")
    public Object getUserInfo(@RequestParam(required = false,value = "id") Long  id){
        User user= clientUserService.findUserById(id);
        return user;
    }

    @ResponseBody
    @RequestMapping("/user/getInfo")
    public Object getUserInfo(){
        String res=clientUserService.findUserById();
        return res;
    }


    @ResponseBody
    @RequestMapping(value = "/user/getUser")
    public Object findUser(){
        User user=new User();
        user.setUserName("chaohchao");
        user.setPasswd("hhhhhh");
        User res=clientUserService.findUser(user);
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "/config/get")
    public Object configPath(){
        return path;
    }


    @ResponseBody
    @RequestMapping(value = "/user/save")
    public Object saveUser(User user){
        return user;
    }
}
