package com.qmdm.clinet;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by chenjin on 17/12/18.
 */

@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
public class ClientApplication {

    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }


    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        RestTemplate restTemplate=  new RestTemplate();
        return restTemplate;
    }

    @Bean
    public AlwaysSampler defaultSampler(){
        return new AlwaysSampler();
    }

}
