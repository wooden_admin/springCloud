package com.qmdm.clinet.service;


//import com.qmdm.clinet.service.hystirc.UserServiceImplHystric;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.FeignClientProperties;

/**
 * Created by chenjin on 17/12/18.
 */
//@FeignClient(value = "api-service",configuration = FeignConfig.class,fallback = UserServiceImplHystric.class)
@FeignClient(value = "api-service", configuration = FeignClientProperties.FeignClientConfiguration.class)
public interface ClientUserService extends com.qmdm.api.service.UserService {

}
