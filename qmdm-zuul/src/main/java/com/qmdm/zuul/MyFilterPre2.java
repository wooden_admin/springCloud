package com.qmdm.zuul;

import com.netflix.zuul.ZuulFilter;
import org.springframework.stereotype.Component;

/**
 * Created by chenjin on 18/1/15.
 */
@Component
public class MyFilterPre2 extends ZuulFilter {


    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 2;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {

        System.out.println("MyFilterPre2 is runing.....");
        return null;
    }
}
