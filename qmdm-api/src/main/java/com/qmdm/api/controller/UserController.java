package com.qmdm.api.controller;

import com.qmdm.api.service.UserService;
import com.qmdm.api.pojo.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by chenjin on 17/12/18.
 */
@RestController
public class UserController {

    @Resource
    public UserService userService;

    @RequestMapping(value = "/user/gets",method = RequestMethod.POST)
    public User findById(@RequestBody Long id){
        return userService.findUserById(id);
    }

    @RequestMapping(value = "/user/getinfos",method = RequestMethod.GET)
    public String findByIdInfo(){
        return userService.findUserById();
    }

    @RequestMapping(value = "/user/getUsers",method = RequestMethod.POST)
    public User findById(@RequestBody User user){
        System.out.println(user.toString());
        return userService.findUser(user);
    }
}
