package com.qmdm.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;

/**
 * Created by chenjin on 17/12/18.
 */
@SpringBootApplication
@EnableEurekaClient
public class APIApplication {

    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(APIApplication.class, args);
    }

    @Bean
    public AlwaysSampler defaultSampler(){
        return new AlwaysSampler();
    }
}
