package com.qmdm.api.pojo;

import java.io.Serializable;

/**
 * Created by chenjin on 17/12/18.
 */
public class User implements Serializable{

    public User (){
    }

    private String userName;


    private String passwd;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", passwd='" + passwd + '\'' +
                '}';
    }
}
