package com.qmdm.api.service.impl;

import com.qmdm.api.service.UserService;
import com.qmdm.api.pojo.User;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by chenjin on 17/12/18.
 */
@Component
public class UserServiceImpl implements UserService {

    @Override
    public User findUserById(Long id) {
        if (id==null){
            User user=new User();
            user.setPasswd("chenjin");
            user.setUserName("chenjin");
            return user;
        }else {
            User user=new User();
            user.setPasswd("jinjin");
            user.setUserName("jinjin");
            return user;
        }
    }

    @Override
    public String findUserById() {
        return "chenjin2321312";
    }

    @Override
    public User findUser(@RequestBody User user) {
        return user;
    }


}
