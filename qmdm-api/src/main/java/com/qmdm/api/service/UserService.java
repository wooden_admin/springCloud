package com.qmdm.api.service;

import com.qmdm.api.pojo.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.FeignClientProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * Created by chenjin on 17/12/18.
 */
@Service
@FeignClient(name = "api-service",
        configuration = FeignClientProperties.FeignClientConfiguration.class)
public interface UserService {

    @RequestMapping(value = "/user/gets",method = RequestMethod.POST)
    public User findUserById(Long id);

    @RequestMapping(value = "/user/getinfos",method = RequestMethod.GET)
    public String findUserById();

    @RequestMapping(value = "/user/getUsers",method = RequestMethod.POST)
    public User findUser(User user);
}
